#our web app framework!

#you could also generate a skeleton from scratch via
#http://flask-appbuilder.readthedocs.io/en/latest/installation.html

#Generating HTML from within Python is not fun, and actually pretty cumbersome because you have to do the
#HTML escaping on your own to keep the application secure. Because of that Flask configures the Jinja2 template engine 
#for you automatically.
#requests are objects that flask handles (get set post, etc)
from flask import Flask, render_template,request, Response,send_from_directory, send_file, redirect, url_for, flash, session
import cv2
#from flask.ext.session import Session
#scientific computing library for saving, reading, and resizing images
#from scipy.misc import imsave, imread, imresize
#for matrix math
import numpy as np
#for importing our keras model
import keras.models
#for regular expressions, saves time dealing with string data
import re
import base64
#system level operations (like loading files)
import sys 
#for reading operating system data
import os
from werkzeug.utils import secure_filename


from keras.preprocessing.image import ImageDataGenerator
# import the necessary packages
from sklearn.cluster import KMeans
import argparse
from flask_uploads import UploadSet, configure_uploads, IMAGES, patch_request_class
import os
from RCNN.mrcnn.visualize import display_top_masks
from flask_dropzone import Dropzone
#predict modules
from colormath.color_objects import sRGBColor, LabColor
from colormath.color_conversions import convert_color
from colormath.color_diff import delta_e_cie2000
import xlsxwriter


from keras.models import load_model

import numpy as np
import skimage.filters as skf
import skimage.color as skc
import skimage.morphology as skm
from skimage.measure import label
import skimage.morphology as skm
from skimage.filters import gaussian
from skimage.color import rgb2hsv
from skimage.util import img_as_float


#tell our app where our saved model is
sys.path.append(os.path.abspath("./model"))
from load import * 
#initalize our flask app
app = Flask(__name__, static_url_path='')



#app.config['SECRET_KEY'] = 'mkodosofa291'
#sess = Session()
#sess.init_app(app)
#global vars for easy reusability
global model, graph, model_vcc
ROOT_DIR = os.getcwd
print("ROOT:", ROOT_DIR)
#initialize these variables
model, graph, model_vcc, graph2, model_modelornot, graph3,model_classifymultiple, graph4 = init()

#decoding an image from base64 into raw representation
# def convertImage(imgData1): 
#     imgstr = re.search(b'base64,(.*)',imgData1).group(1) #print(imgstr) 
#     with open('output.png','wb') as output:
#         output.write(base64.b64decode(imgstr))
UPLOAD_FOLDER = os.getcwd()#'/usr/scr/app'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])


app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS




def centroid_histogram(clt):
    # grab the number of different clusters and create a histogram
    # based on the number of pixels assigned to each cluster
    numLabels = np.arange(0, len(np.unique(clt.labels_)) + 1)
    (hist, _) = np.histogram(clt.labels_, bins = numLabels)
    
    # normalize the histogram, such that it sums to one
    hist = hist.astype("float")
    hist /= hist.sum()
    
    # return the histogram
    return hist


def plot_colors(hist, centroids):
    # initialize the bar chart representing the relative frequency
    # of each of the colors
    bar = np.zeros((50, 300, 3), dtype = "uint8")
    startX = 0
    
    # loop over the percentage of each cluster and the color of
    # each cluster
    for (percent, color) in zip(hist, centroids):
        # plot the relative percentage of each cluster
        endX = startX + (percent * 300)
        cv2.rectangle(bar, (int(startX), 0), (int(endX), 50),
            color.astype("uint8").tolist(), -1)
        startX = endX
    
    # return the bar chart
    return bar

def plot_farben2(image):
    # load the image and convert it from BGR to RGB so that
    # we can dispaly it with matplotlib


    # reshape the image to be a list of pixels
    ## KICK ALL BACKGROUND PIXELS - WE DONT WANT THEM !!

#     img_filter = fg_masked[fg_masked != 0]
    
#     image = np.reshape(img_filter, (-1,3))

    # create the indices that have at least one channel of the pixel different than 0
    indToSelect = np.any(image != 0, keepdims=True, axis=2)[:,:,0] # used [:,:,0] because numpy returns an array of shape (rows, cols, 1)
    # get the 3 channel values of the selected values above
    forKmeans = image[indToSelect, :]

    # cluster the pixel intensities
    clt = KMeans(n_clusters =2, n_jobs=1,random_state=42)
    clt.fit(forKmeans)


    # build a histogram of clusters and then create a figure
    # representing the number of pixels labeled to each color

    hist = centroid_histogram(clt)
    bar = plot_colors(hist, clt.cluster_centers_)

    # show our color bart
    #plt.figure()
    # f, (ax1) = plt.subplots(1, 1, sharey=False)
    # plt.axis("off")
    # ax1.imshow(bar)
    # plt.show()
    return bar, clt.cluster_centers_, hist


################################ COLOR NAMES #################################################
import sys
import re
from types import *
import operator

class BadColor(Exception):
    pass

DEFAULT_DB = None
SPACE = ' '
COMMASPACE = ', '


# generic class
class ColorDB:
    def __init__(self, fp):
        lineno = 2
        self.__name = fp.name
        # Maintain several dictionaries for indexing into the color database.
        # Note that while Tk supports RGB intensities of 4, 8, 12, or 16 bits,
        # for now we only support 8 bit intensities.  At least on OpenWindows,
        # all intensities in the /usr/openwin/lib/rgb.txt file are 8-bit
        #
        # key is (red, green, blue) tuple, value is (name, [aliases])
        self.__byrgb = {}
        # key is name, value is (red, green, blue)
        self.__byname = {}
        # all unique names (non-aliases).  built-on demand
        self.__allnames = None
        for line in fp:
            # get this compiled regular expression from derived class
            mo = self._re.match(line)
            if not mo:
                print >> sys.stderr, 'Error in', fp.name, ' line', lineno
                lineno += 1
                continue
            # extract the red, green, blue, and name
            red, green, blue = self._extractrgb(mo)
            name = self._extractname(mo)
            keyname = name.lower()
            # BAW: for now the `name' is just the first named color with the
            # rgb values we find.  Later, we might want to make the two word
            # version the `name', or the CapitalizedVersion, etc.
            key = (red, green, blue)
            foundname, aliases = self.__byrgb.get(key, (name, []))
            if foundname != name and foundname not in aliases:
                aliases.append(name)
            self.__byrgb[key] = (foundname, aliases)
            # add to byname lookup
            self.__byname[keyname] = key
            lineno = lineno + 1

    # override in derived classes
    def _extractrgb(self, mo):
        return [int(x) for x in mo.group('red', 'green', 'blue')]

    def _extractname(self, mo):
        return mo.group('name')

    def filename(self):
        return self.__name

    def find_byrgb(self, rgbtuple):
        """Return name for rgbtuple"""
        try:
            return self.__byrgb[rgbtuple]
        except KeyError:
            raise BadColor(rgbtuple)

    def find_byname(self, name):
        """Return (red, green, blue) for name"""
        name = name.lower()
        try:
            return self.__byname[name]
        except KeyError:
            raise BadColor(name)

    def nearest(self, red, green, blue):
        """Return the name of color nearest (red, green, blue)"""
        # BAW: should we use Voronoi diagrams, Delaunay triangulation, or
        # octree for speeding up the locating of nearest point?  Exhaustive
        # search is inefficient, but seems fast enough.
        nearest = -1
        nearest_name = ''
        for name, aliases in self.__byrgb.values():
            r, g, b = self.__byname[name.lower()]
            rdelta = red - r
            gdelta = green - g
            bdelta = blue - b
            distance = rdelta * rdelta + gdelta * gdelta + bdelta * bdelta
            if nearest == -1 or distance < nearest:
                nearest = distance
                nearest_name = name
        return nearest_name

    def unique_names(self):
        # sorted
        if not self.__allnames:
            self.__allnames = []
            for name, aliases in self.__byrgb.values():
                self.__allnames.append(name)
            # sort irregardless of case
            def nocase_cmp(n1, n2):
                return cmp(n1.lower(), n2.lower())
            self.__allnames.sort(nocase_cmp)
        return self.__allnames

    def aliases_of(self, red, green, blue):
        try:
            name, aliases = self.__byrgb[(red, green, blue)]
        except KeyError:
            raise BadColor((red, green, blue))
        return [name] + aliases


class RGBColorDB(ColorDB):
    _re = re.compile(
        '\s*(?P<red>\d+)\s+(?P<green>\d+)\s+(?P<blue>\d+)\s+(?P<name>.*)')


class HTML40DB(ColorDB):
    _re = re.compile('(?P<name>\S+)\s+(?P<hexrgb>#[0-9a-fA-F]{6})')

    def _extractrgb(self, mo):
        return rrggbb_to_triplet(mo.group('hexrgb'))

class LightlinkDB(HTML40DB):
    _re = re.compile('(?P<name>(.+))\s+(?P<hexrgb>#[0-9a-fA-F]{6})')

    def _extractname(self, mo):
        return mo.group('name').strip()

class WebsafeDB(ColorDB):
    _re = re.compile('(?P<hexrgb>#[0-9a-fA-F]{6})')

    def _extractrgb(self, mo):
        return rrggbb_to_triplet(mo.group('hexrgb'))

    def _extractname(self, mo):
        return mo.group('hexrgb').upper()



# format is a tuple (RE, SCANLINES, CLASS) where RE is a compiled regular
# expression, SCANLINES is the number of header lines to scan, and CLASS is
# the class to instantiate if a match is found

FILETYPES = [
    (re.compile('Xorg'), RGBColorDB),
    (re.compile('XConsortium'), RGBColorDB),
    (re.compile('HTML'), HTML40DB),
    (re.compile('lightlink'), LightlinkDB),
    (re.compile('Websafe'), WebsafeDB),
    ]

def get_colordb(file, filetype=None):
    colordb = None
    fp = open(file)
    try:
        line = fp.readline()
        if not line:
            return None
        # try to determine the type of RGB file it is
        if filetype is None:
            filetypes = FILETYPES
        else:
            filetypes = [filetype]
        for typere, class_ in filetypes:
            mo = typere.search(line)
            if mo:
                break
        else:
            # no matching type
            return None
        # we know the type and the class to grok the type, so suck it in
        colordb = class_(fp)
    finally:
        fp.close()
    # save a global copy
    global DEFAULT_DB
    DEFAULT_DB = colordb
    return colordb



_namedict = {}

def rrggbb_to_triplet(color):
    """Converts a #rrggbb color to the tuple (red, green, blue)."""
    rgbtuple = _namedict.get(color)
    if rgbtuple is None:
        if color[0] != '#':
            raise BadColor(color)
        red = color[1:3]
        green = color[3:5]
        blue = color[5:7]
        rgbtuple = int(red, 16), int(green, 16), int(blue, 16)
        _namedict[color] = rgbtuple
    return rgbtuple


_tripdict = {}
def triplet_to_rrggbb(rgbtuple):
    """Converts a (red, green, blue) tuple to #rrggbb."""
    global _tripdict
    hexname = _tripdict.get(rgbtuple)
    if hexname is None:
        hexname = '#%02x%02x%02x' % rgbtuple
        _tripdict[rgbtuple] = hexname
    return hexname


_maxtuple = (256.0,) * 3
def triplet_to_fractional_rgb(rgbtuple):
    return map(operator.__div__, rgbtuple, _maxtuple)


def triplet_to_brightness(rgbtuple):
    # return the brightness (grey level) along the scale 0.0==black to
    # 1.0==white
    r = 0.299
    g = 0.587
    b = 0.114
    return r*rgbtuple[0] + g*rgbtuple[1] + b*rgbtuple[2]




colordb = get_colordb('rgb.txt')
if not colordb:
    print('No parseable color database found' )
    sys.exit(1)
# on my system, this color matches exactly
# target = 'navy'
# red, green, blue = rgbtuple = colordb.find_byname(target)
# print( target, ':', red, green, blue, triplet_to_rrggbb(rgbtuple))
# name, aliases = colordb.find_byrgb(rgbtuple)
# print('name:', name, 'aliases:', COMMASPACE.join(aliases))
# r, g, b = (1, 1, 128)                         # nearest to navy
# r, g, b = (145, 238, 144)                     # nearest to lightgreen
# r, g, b = (255, 251, 250)                     # snow
# print('finding nearest to', target, '...') 
# import time
# t0 = time.time()
# nearest = colordb.nearest(r, g, b)
# t1 = time.time()
# print('found nearest color', nearest, 'in', t1-t0, 'seconds') 
######################################################################################################


################## COLORS FROM WEBCOLORS #######################


import webcolors

def closest_colour(requested_colour):
    min_colours = {}
    for key, name in webcolors.html4_hex_to_names.items():
        r_c, g_c, b_c = webcolors.hex_to_rgb(key)
        rd = (r_c - requested_colour[0]) ** 2
        gd = (g_c - requested_colour[1]) ** 2
        bd = (b_c - requested_colour[2]) ** 2
        min_colours[(rd + gd + bd)] = name
    return min_colours[min(min_colours.keys())]

def get_colour_name(requested_colour):
    try:
        closest_name = actual_name = webcolors.rgb_to_name(requested_colour,spec='html4')
    except ValueError:
        closest_name = closest_colour(requested_colour)
        actual_name = None
    return actual_name, closest_name

# requested_colour = (119.5, 172.9, 152)
# actual_name, closest_name = get_colour_name(requested_colour)

# print("Actual colour name:", actual_name, ", closest colour name:", closest_name)



#################################################################




################ DROPZONE CODE ################################################################

    
dropzone = Dropzone(app)

# Dropzone settings
app.config['DROPZONE_UPLOAD_MULTIPLE'] = True
app.config['DROPZONE_ALLOWED_FILE_CUSTOM'] = True
app.config['DROPZONE_ALLOWED_FILE_TYPE'] = 'image/*'
app.config['DROPZONE_REDIRECT_VIEW'] = 'results'




# Uploads settings
app.config['UPLOADED_PHOTOS_DEST'] = os.getcwd() + '/image_uploads/images'
print("CURRENT PATH: ",os.getcwd())
photos = UploadSet('photos', IMAGES)
configure_uploads(app, photos)
patch_request_class(app)  # set maximum file size, default is 16MB

    
app.config['SECRET_KEY'] = 'supersecretkeygoeshere'

@app.route('/return-file/')
def download_file():
    return send_file(os.getcwd() + "/images.xlsx", as_attachment=True)


@app.route('/', methods=['GET', 'POST'])
def index():
    global progress_bar
    progress_bar = 0
    
    # set session for image results
    if "file_urls" not in session:
        session['file_urls'] = []
        session['filenames'] = []
    # list to hold our uploaded image urls
    file_urls = session['file_urls']
    datei_name = session['filenames'] 
    # handle image upload from Dropzone
    if request.method == 'POST':
        file_obj = request.files
        print(file_obj)
        for f in file_obj:
            file = request.files.get(f)
            # save the file with to our photos folder
            filename = photos.save(
                file,
                name=file.filename    
            )
            # append image urls
            file_urls.append(photos.url(filename))
            datei_name.append(file.filename) 

        
         
        download_file() 
        session['file_urls'] = file_urls
        session['filenames'] = datei_name

        return "uploading..."
    #predict(file_urls, datei_name) 
    folder = os.getcwd()+'/image_uploads/images'
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            #elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print(e)
    folder = os.getcwd()+'/farb_plots'
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            #elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print(e)
    # return dropzone template on GET request    
    return render_template('index.html')


@app.route('/results')
def results():
    
    # redirect to home if no images to display
    if "file_urls" not in session or session['file_urls'] == []:
        return redirect(url_for('index'))
        
    # set the file_urls and remove the session variable
    file_urls = session['file_urls']
    session.pop('file_urls', None)
    
    return render_template('results.html', file_urls=file_urls)

################################################################################################






@app.route("/getPlotCSV")
def getPlotCSV():
    # with open("outputs/Adjacency.csv") as fp:
    #     csv = fp.read()
    csv = '1,2,3\n4,5,6\n'
    return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":
                 "attachment; filename=myplot.csv"})
        

@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)




@app.route('/progress')
def ajax_index():
    if progress_bar != 0:
        i = progress_bar
    else:
        i = 0
    print(progress_bar)
    # for i in range(500):
    #     print("%d" % i)
    return str(i)

def get_colorbar(final_img_masked):
    bar, center, hist = plot_farben2(final_img_masked)
    relevant_color = []
    s = 0
    for x in hist:
        if x > .25:
            relevant_color.append(center[s])
            if x > .69:
                relevant_color = []
                relevant_color.append(center[s])
                break
        s += 1


    if len(relevant_color) == 2:
         # Color 1
        color1_rgb = sRGBColor(*relevant_color[0]);
        # Color 2
        color2_rgb = sRGBColor(*relevant_color[1]);
        # Convert from RGB to Lab Color Space
        color1_lab = convert_color(color1_rgb, LabColor);
        # Convert from RGB to Lab Color Space
        color2_lab = convert_color(color2_rgb, LabColor);
        # Find the color difference
        delta_e = delta_e_cie2000(color1_lab, color2_lab);
        if delta_e > 25: ### Toleranz, je hoeher desto krasser muss der Unterschied der Farben sein damit sie noch als 2 Farben betrachtet werden
        
            print("2 farben")
            print(relevant_color)
            selected_colors = plot_colors([.5,.5], relevant_color)
            rgb_code = relevant_color
        else:
            print("color zu aehnlich")
            print(np.float32(relevant_color[np.argmax(hist)]))
            selected_colors = plot_colors([1.0], [relevant_color[np.argmax(hist)]]) # select color which is most prominent, forget about the other similar one
            rgb_code = relevant_color
    if len(relevant_color) == 1:
        selected_colors = plot_colors([1.0], relevant_color)
        rgb_code = relevant_color
    selected_colors = cv2.cvtColor(selected_colors,cv2.COLOR_BGR2RGB)
    return selected_colors, rgb_code 

@app.route('/predict',methods=['GET'])    
def predict():
    #print(filenames)
    global progress_bar 
    
    import os
    files_name = []
    files_path = []
    files_in_dir = []
    print("curretn working dir:", os.getcwd())
    images_path = './image_uploads/images'
    print(images_path)
    anzahl_bilder = len(os.listdir(images_path))
    increase_progressbar_per_step = 100 / anzahl_bilder

    for x in os.listdir(images_path):
        if x != ".DS_Store":
            files_name.append(x)
            files_path.append(os.path.join(images_path, x))




    test_filenames = files_path
    print('-----------------')
    print(test_filenames)
    print('-----------------')


    workbook = xlsxwriter.Workbook('images.xlsx')
    worksheet = workbook.add_worksheet()
    worksheet.set_column('E:E', 20)
    j = 1
    counter_class_index = 0
    
    for i in test_filenames:
        if counter_class_index == 0:
           progress_bar += (increase_progressbar_per_step * 2) 
        else:
            progress_bar += increase_progressbar_per_step
        render_template('results.html', variable=str(i))
        print(i)
        test_bild = cv2.imread(str(i))
        zwischen_bild = cv2.resize(test_bild, (200,200))
        cv2.imwrite('./farb_plots/Image_{}.jpg'.format(j), zwischen_bild) 

        test_bild = cv2.cvtColor(test_bild,cv2.COLOR_BGR2RGB)


                ################ BILD MIT MODEL? Pruefe ob das Bild ein Modell beinhaltet - wenn ja dann lass das Model mit 4 Klassen entscheiden, sonst das Modell mit 27 Klassen
        def load_image(img_path, show=False):
            from keras.preprocessing import image
            img = image.load_img(img_path, target_size=(224, 224))
            img_tensor = image.img_to_array(img)                    # (height, width, channels)
            img_tensor = np.expand_dims(img_tensor, axis=0)         # (1, height, width, channels), add a dimension because the model expects this shape: (batch_size, height, width, channels)
            #img_tensor /= 255.                                      # imshow expects values in the range [0, 1]

            if show:
                plt.imshow(img_tensor[0])                           
                plt.axis('off')
                plt.show()

            return img_tensor

        model_or_not_image = load_image(i)
        with graph3.as_default():
            predicted_class = model_modelornot.predict(model_or_not_image)
        if np.argmax(predicted_class) == 0: ### 1 = no model, 0 = model in picture
            print("Model im Bild")


                #in our computation graph
            
            #perform the prediction
            # Versuche ein (bekanntes /gelerntes )Item im Bild zu identifizieren. Falls keins erkannt wird, speichere N/A in der Farbspalte - hier einfach den KMeans mask algo einsetzen
            try:
                with graph.as_default():
                    results = model.detect([test_bild], verbose=1)




                r = results[0]
                print("schritt 1")
                result = visualize.display_top_masks(test_bild, r['masks'], r['class_ids'], 
                                            "top")
                rcnn_mask = np.int8(result[1])
                
                final_img_masked = cv2.bitwise_and(test_bild,test_bild, mask=rcnn_mask)
                



                selected_colors, rgb_code = get_colorbar(final_img_masked)
                color_names = []
                if len(rgb_code) > 1:
                    for color_values in rgb_code:
                        color_values = tuple(color_values.tolist())
                        color_names.append(color_values)

                        print(color_values)
                        actual_name, closest_name = get_colour_name(*color_values)
                        #color_name = (colordb.nearest(*color_values))
                        color_name = closest_name
                else:
                    color_values = tuple(rgb_code[0].tolist())
                    actual_name, closest_name = get_colour_name(*color_values)
                    #color_name = colordb.nearest(*color_values)
                    color_name = closest_name
                print(rgb_code)

                print("schritt 2")
                #plt.savefig("./farb_plots/myplot_{}.png".format(j), dpi = 50)
                # Insert an image.
                cv2.imwrite("./farb_plots/bar_{}.jpg".format(j),selected_colors)
                worksheet.insert_image('F'+str(j+4), "./farb_plots/bar_{}.jpg".format(j),{'x_scale': 0.5, 'y_scale': 0.5})
                worksheet.insert_image('A'+str(j), './farb_plots/Image_{}.jpg'.format(j))
                print("doing it")
                #bild_dateiname = i.split(dir_path)[1][1:]
                bild_dateiname = files_name[counter_class_index]
                worksheet.write('E'+str(j+4), bild_dateiname)
                worksheet.write('K'+str(j+4), color_name)
                #owrksheet.insert_image('F'+str(j), "./farb_plots/myplot_{}.png".format(j),{'x_scale': 0.5, 'y_scale': 0.5})
                
            except:
                ##### Hier besser die Mask aus KNN nutzen, um die Farbe dennoch zu bestimmen. 
                print("Klasse wurde nicht erkannt von RCNN")


                ###### MASK


                def glob(img):
                    h, w = img.shape[:2]
                    mask = np.zeros((h, w), dtype=np.bool)
                    maxdistance = 5 #Default 5

                    #if self.settings['uselab']:
                    img = skc.rgb2lab(img)

                    # Compute euclidean distance of each corner against all other pixels.
                    corners = [(0, 0), (-1, 0), (0, -1), (-1, -1)]
                    for color in (img[i, j] for i, j in corners):
                        norm = np.sqrt(np.sum(np.square(img - color), 2))
                        # Add to the mask pixels close to one of the corners.
                        mask |= norm < maxdistance

                    return mask

                def floodfill(img):
                    back = scharr(img)
                    # Binary thresholding.
                    back = back > 0.05

                    # Thin all edges to be 1-pixel wide.
                    back = skm.skeletonize(back)

                    # Edges are not detected on the borders, make artificial ones.
                    back[0, :] = back[-1, :] = True
                    back[:, 0] = back[:, -1] = True

                    # Label adjacent pixels of the same color.
                    labels = label(back, background=-1, connectivity=1)

                    # Count as background all pixels labeled like one of the corners.
                    corners = [(1, 1), (-2, 1), (1, -2), (-2, -2)]
                    for l in (labels[i, j] for i, j in corners):
                        back[labels == l] = True

                    # Remove remaining inner edges.
                    return skm.opening(back)

                def defaultsettings():
                    return {
                        'maxdistance': 5,
                        'uselab': True,
                    }

                def scharr(img):
                    # Invert the image to ease edge detection.
                    img = 1. - img
                    grey = skc.rgb2grey(img)
                    return skf.scharr(grey)

                def get(img):
                    f = floodfill(img)
                    g = glob(img)
                    m = f | g

                    if np.count_nonzero(m) < 0.90 * m.size:
                        return m

                    ng = np.count_nonzero(g)
                    nf = np.count_nonzero(f)

                    if ng < 0.90 * g.size and nf < 0.90 * f.size:
                        return g if ng > nf else f

                    if ng < 0.90 * g.size:
                        return g

                    if nf < 0.90 * f.size:
                        return f

                    return np.zeroslike(m)
                ### SKIN FILTER
                _k = skm.disk(1, np.bool)

                lo = np.array([0, 0.19, 0.31], np.float64)
                up = np.array([0.1, 1., 1.], np.float64)
                #         elif t != 'none':
                #             raise NotImplementedError('Only general type is implemented')
                def range_mask(img):
                    mask = np.all((img >= lo) & (img <= up), axis=2)

                    # Smooth the mask.
                    skm.binary_opening(mask, selem=_k, out=mask)
                    return gaussian(mask, 0.8, multichannel=True) != 0

                def get_skin(img):
                    t = 'general'
                    if t == 'general':
                        img = rgb2hsv(img)
                    elif t == 'none':
                        return np.zeros(img.shape[:2], np.bool)
                    else:
                        raise NotImplementedError('Only general type is implemented')

                    return range_mask(img)

                ######## BILD MASKIEREN
                
                mask = get(test_bild)
                mask = np.int8(mask)
                #mask = mask.dtype='uint8'
                #mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
                mask[mask == 0] = 255
                mask[mask == 1] = 0
                mask[mask == 255] = 1
                #fg_masked = cv2.bitwise_or(test_bild, test_bild, mask=mask)
                #plt.imshow(fg_masked)

                # get rid of skin
                skin_mask = get_skin(test_bild)
                skin_mask = np.int8(skin_mask)
                skin_mask[skin_mask == 0] = 255
                skin_mask[skin_mask == 1] = 0
                skin_mask[skin_mask == 255] = 1
                #fg_masked = cv2.bitwise_and(fg_masked, fg_masked, mask=skin_mask)
                fg_masked = cv2.bitwise_and(mask, mask, mask=skin_mask)

                # loesche maske aus original bild
                final_img = cv2.bitwise_or(test_bild, test_bild, mask=fg_masked)

                selected_colors, rgb_code = get_colorbar(final_img)
                color_names = []
                if len(rgb_code) > 1:
                    for color_values in rgb_code:
                        color_values = tuple(color_values.tolist())
                        color_names.append(color_values)
                        print("hello2")
                        print(color_values)
                        actual_name, closest_name = get_colour_name(*color_values)
                        #color_name = colordb.nearest(*color_values)
                        color_name = closest_name
                else:
                    color_values = tuple(rgb_code[0].tolist())
                    print("hello")
                    actual_name, closest_name = get_colour_name(*color_values)
                    #color_name = colordb.nearest(*color_values)
                    color_name = closest_name


                cv2.imwrite("./farb_plots/bar_{}.jpg".format(j),selected_colors)
                worksheet.insert_image('F'+str(j+4), "./farb_plots/bar_{}.jpg".format(j),{'x_scale': 0.5, 'y_scale': 0.5})


                worksheet.insert_image('A'+str(j), './farb_plots/Image_{}.jpg'.format(j))
                bild_dateiname = files_name[counter_class_index]
                worksheet.write('E'+str(j+4), bild_dateiname)
                worksheet.write('K'+str(j+4), color_name)
                #owrksheet.insert_image('F'+str(j), "./farb_plots/myplot_{}.png".format(j),{'x_scale': 0.5, 'y_scale': 0.5})
                ### GGF NICHT EINDEUTIG EINFUEGEN ALS KLASSE WENN DIESE METHODE FEUERT




            ############################# VGG 16 - Kennt nur 4 Klassen
            from keras.preprocessing import image
            def load_image(img_path, show=False):

                img = image.load_img(img_path, target_size=(224, 224))
                img_tensor = image.img_to_array(img)                    # (height, width, channels)
                img_tensor = np.expand_dims(img_tensor, axis=0)         # (1, height, width, channels), add a dimension because the model expects this shape: (batch_size, height, width, channels)
                img_tensor /= 255.                                      # imshow expects values in the range [0, 1]

                if show:
                    plt.imshow(img_tensor[0])                           
                    plt.axis('off')
                    plt.show()

                return img_tensor

            vgg_image = load_image(i)
            with graph2.as_default():
                #predictions_prob = model_vcc.predict_generator(train_batches,verbose=1)
                predictions_prob = model_vcc.predict(vgg_image)


            classes = {'hemd': 0, 'jeans': 1, 'pullover': 2, 'shirt': 3}

            #predictions.append(list(classes.keys())[list(classes.values()).index(np.argmax(x))] )
            predicted_label = list(classes.keys())[list(classes.values()).index(np.argmax(predictions_prob))]
            worksheet.write('I'+str(j+4), predicted_label)

        #######################################

        ############################# VGG 16 - Kennt 27 Klassen

        else:
            print("Kein Model im Bild")           
            from keras.preprocessing import image
            def load_image(img_path, show=False):

                img = image.load_img(img_path, target_size=(224, 224))
                img_tensor = image.img_to_array(img)                    # (height, width, channels)
                img_tensor = np.expand_dims(img_tensor, axis=0)         # (1, height, width, channels), add a dimension because the model expects this shape: (batch_size, height, width, channels)
                img_tensor /= 255.                                      # imshow expects values in the range [0, 1]

                if show:
                    plt.imshow(img_tensor[0])                           
                    plt.axis('off')
                    plt.show()

                return img_tensor

            vgg_image = load_image(i)
            with graph4.as_default():
                #predictions_prob = model_vcc.predict_generator(train_batches,verbose=1)
                predictions_prob = model_classifymultiple.predict(vgg_image)

            # KLASSEN ANPASSEN# KLASSEN ANPASSEN# KLASSEN ANPASSEN# KLASSEN ANPASSEN# KLASSEN ANPASSEN    
            classes = {'Bluse': 0,
                         'Cardigan': 1,
                         'Trouser': 2,
                         'Coat': 3,
                         'dresses': 4,
                         'jackets': 5,
                         'jeans': 6,
                         'jeans_shorts': 7,
                         'jogging_pants': 8,
                         'kimonos': 9,
                         'leggins': 10,
                         'light_jackets_blazer': 11,
                         'longsleeves': 12,
                         'onesies': 13,
                         'poloshirts': 14,
                         'pullover': 15,
                         'shirts': 16,
                         'shorts': 17,
                         'skirts': 18,
                         'sports_pants': 19,
                         'suit_pants': 20,
                         'suit_vests': 21,
                         'suits': 22,
                         'tops': 23,
                         'tshirts': 24,
                         'tunics': 25,
                         'vests': 26} 
                    
            def glob(img):
                h, w = img.shape[:2]
                mask = np.zeros((h, w), dtype=np.bool)
                maxdistance = 5 #Default 5

                #if self.settings['uselab']:
                img = skc.rgb2lab(img)

                # Compute euclidean distance of each corner against all other pixels.
                corners = [(0, 0), (-1, 0), (0, -1), (-1, -1)]
                for color in (img[i, j] for i, j in corners):
                    norm = np.sqrt(np.sum(np.square(img - color), 2))
                    # Add to the mask pixels close to one of the corners.
                    mask |= norm < maxdistance

                return mask

            def floodfill(img):
                back = scharr(img)
                # Binary thresholding.
                back = back > 0.05

                # Thin all edges to be 1-pixel wide.
                back = skm.skeletonize(back)

                # Edges are not detected on the borders, make artificial ones.
                back[0, :] = back[-1, :] = True
                back[:, 0] = back[:, -1] = True

                # Label adjacent pixels of the same color.
                labels = label(back, background=-1, connectivity=1)

                # Count as background all pixels labeled like one of the corners.
                corners = [(1, 1), (-2, 1), (1, -2), (-2, -2)]
                for l in (labels[i, j] for i, j in corners):
                    back[labels == l] = True

                # Remove remaining inner edges.
                return skm.opening(back)

            def defaultsettings():
                return {
                    'maxdistance': 5,
                    'uselab': True,
                }

            def scharr(img):
                # Invert the image to ease edge detection.
                img = 1. - img
                grey = skc.rgb2grey(img)
                return skf.scharr(grey)

            def get(img):
                f = floodfill(img)
                g = glob(img)
                m = f | g

                if np.count_nonzero(m) < 0.90 * m.size:
                    return m

                ng = np.count_nonzero(g)
                nf = np.count_nonzero(f)

                if ng < 0.90 * g.size and nf < 0.90 * f.size:
                    return g if ng > nf else f

                if ng < 0.90 * g.size:
                    return g

                if nf < 0.90 * f.size:
                    return f

                return np.zeroslike(m)
            ### SKIN FILTER
            _k = skm.disk(1, np.bool)

            lo = np.array([0, 0.19, 0.31], np.float64)
            up = np.array([0.1, 1., 1.], np.float64)
            #         elif t != 'none':
            #             raise NotImplementedError('Only general type is implemented')
            def range_mask(img):
                mask = np.all((img >= lo) & (img <= up), axis=2)

                # Smooth the mask.
                skm.binary_opening(mask, selem=_k, out=mask)
                return gaussian(mask, 0.8, multichannel=True) != 0

            def get_skin(img):
                t = 'general'
                if t == 'general':
                    img = rgb2hsv(img)
                elif t == 'none':
                    return np.zeros(img.shape[:2], np.bool)
                else:
                    raise NotImplementedError('Only general type is implemented')

                return range_mask(img)

            ######## BILD MASKIEREN
            
            mask = get(test_bild)
            mask = np.int8(mask)
            #mask = mask.dtype='uint8'
            #mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
            mask[mask == 0] = 255
            mask[mask == 1] = 0
            mask[mask == 255] = 1
            #fg_masked = cv2.bitwise_or(test_bild, test_bild, mask=mask)
            #plt.imshow(fg_masked)

            # get rid of skin
            skin_mask = get_skin(test_bild)
            skin_mask = np.int8(skin_mask)
            skin_mask[skin_mask == 0] = 255
            skin_mask[skin_mask == 1] = 0
            skin_mask[skin_mask == 255] = 1
            #fg_masked = cv2.bitwise_and(fg_masked, fg_masked, mask=skin_mask)
            fg_masked = cv2.bitwise_and(mask, mask, mask=skin_mask)

            # lösche maske aus original bild
            final_img = cv2.bitwise_or(test_bild, test_bild, mask=fg_masked)

            selected_colors, rgb_code = get_colorbar(final_img)
            color_names = []
            if len(rgb_code) > 1:
                for color_values in rgb_code:
                    color_values = tuple(color_values.tolist())
                    color_names.append(color_values)
                    print("hello2")
                    print(color_values)
                    actual_name, closest_name = get_colour_name(*color_values)
                    #color_name = colordb.nearest(*color_values)
                    color_name = closest_name
                    #actual_name, color_name = get_colour_name((color_values))
            else:
                color_values = tuple(rgb_code[0].tolist())
                print("hello")
                ctual_name, closest_name = get_colour_name(*color_values)
                #color_name = colordb.nearest(*color_values)
                color_name = closest_name
                #actual_name, color_name = get_colour_name((color_values))


            #owrksheet.insert_image('F'+str(j), "./farb_plots/myplot_{}.png".format(j),{'x_scale': 0.5, 'y_scale': 0.5})
            ### GGF NICHT EINDEUTIG EINFÜGEN ALS KLASSE WENN DIESE METHODE FEUERT



            #predictions.append(list(classes.keys())[list(classes.values()).index(np.argmax(x))] )
            predicted_label = list(classes.keys())[list(classes.values()).index(np.argmax(predictions_prob))]
            worksheet.write('I'+str(j+4), predicted_label)
            cv2.imwrite("./farb_plots/bar_{}.jpg".format(j),selected_colors)
            worksheet.insert_image('F'+str(j+4), "./farb_plots/bar_{}.jpg".format(j),{'x_scale': 0.5, 'y_scale': 0.5})


            worksheet.insert_image('A'+str(j), './farb_plots/Image_{}.jpg'.format(j))
            bild_dateiname = files_name[counter_class_index]
            worksheet.write('E'+str(j+4), bild_dateiname)
            worksheet.write('K'+str(j+4), color_name)
            #######################################


        j += 10
        counter_class_index += 1
    
    ########################## VGG
    import os, shutil
    #shutil.copyfile(images_path, os.getcwd()+ "/predictions")
    # tf.reset_default_graph()
    # train_batches = ImageDataGenerator(rescale=1.0/255).flow_from_directory(os.getcwd()+"/image_uploads", target_size=(224,224),  batch_size=1,shuffle=False)
    # with graph2.as_default():
    #     predictions_prob = model_vcc.predict_generator(train_batches,verbose=1)
    # classes = {'hemden': 0, 'jeans': 1, 'pullover': 2, 'shirts': 3}
    # predictions = []
    # counter = 0
    # for x in predictions_prob:
    #     #predictions.append(list(classes.keys())[list(classes.values()).index(np.argmax(x))] )
    #     predicted_label = list(classes.keys())[list(classes.values()).index(np.argmax(x))]
    #     worksheet.write('I'+str(counter+5), predicted_label)
    #     counter += 10
    ###############################



    workbook.close()

    
    folder = os.getcwd()+'/image_uploads/images'
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            #elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print(e)
    folder = os.getcwd()+'/farb_plots'
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            #elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print(e)

    # folder = os.getcwd()+ "/predictions/image_uploads"
    # for the_file in os.listdir(folder):
    #     file_path = os.path.join(folder, the_file)
    #     try:
    #         if os.path.isfile(file_path):
    #             os.unlink(file_path)
    #         #elif os.path.isdir(file_path): shutil.rmtree(file_path)
    #     except Exception as e:
    #         print(e)
    print("workbook saved")
    #uploads = '/usr/scr/app/images.xlsx'
    uploads = '/app'#os.path.join(app.root_path, '/')
    print(uploads)
    #return send_file(uploads, as_attachment=True)
    #return send_from_directory(directory=uploads, filename='images.xlsx',as_attachment=True)
    return render_template('download.html')




if __name__ == "__main__":

    #app.config['SESSION_TYPE'] = 'filesystem'

    #sess.init_app(app)

    #decide what port to run the app in
    #port = int(os.environ.get('PORT', 8080))
    #run the app locally on the givn port
    #port = int(os.environ.get("PORT",5000))
    app.run(host='0.0.0.0', port=80, debug=True)
    #app.run(host='0.0.0.0',port=port)
    #optional if we want to run in debugging mode
    #app.run(debug=True)
