FROM tiangolo/uwsgi-nginx-flask:python3.6




ADD ./model/requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

ADD . /usr/scr/app
WORKDIR /usr/scr/app


CMD gunicorn -w 1 --bind 0.0.0.0:$PORT wsgi




