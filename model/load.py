# import numpy as np
# import keras.models
# from keras.models import model_from_json
# from scipy.misc import imread, imresize,imshow
# import tensorflow as tf
#system level operations (like loading files)
import sys 
#for reading operating system data
import os
sys.path.append(os.path.abspath("./RCNN"))
from mrcnn import utils
from mrcnn import visualize
from mrcnn.visualize import display_images
from mrcnn.visualize import display_top_masks
import mrcnn.model as modellib
from mrcnn.model import log
import os
import sys
import random
import math
import re
import time
import numpy as np
import tensorflow as tf
#import matplotlib
#import matplotlib.pyplot as plt
#import matplotlib.patches as patches
from samples.balloon import balloon
from keras.models import load_model


def init(): 
	# json_file = open('model.json','r')
	# loaded_model_json = json_file.read()
	# json_file.close()
	# loaded_model = model_from_json(loaded_model_json)
	# #load woeights into new model
	# loaded_model.load_weights("model.h5")
	# print("Loaded Model from disk")

	# #compile and evaluate loaded model
	# loaded_model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=['accuracy'])
	# #loss,accuracy = model.evaluate(X_test,y_test)
	# #print('loss:', loss)
	# #print('accuracy:', accuracy)
	# graph = tf.get_default_graph()

	# Modell checkt ob ein Model im Bild ist oder nicht
	model_modelornot = load_model('resnet50_best.h5')
	graph3 = tf.get_default_graph()

	# Modell klassifiziert Freisteller - 27 Klassen 
	model_classifymultiple = load_model('27_vgg16_model_all.h5')
	graph4 = tf.get_default_graph()

	# Modell klassifiziert Bilder mit Models - 4 Klassen
	model_vcc = load_model('fast_4_vgg16_model.h5')# returns a compiled model
	graph2 = tf.get_default_graph()

	config = balloon.BalloonConfig()

	# Device to load the neural network on.
	# Useful if you're training a model on the same 
	# machine, in which case use CPU and leave the
	# GPU for training.
	DEVICE = "/cpu:0"  # /cpu:0 or /gpu:0

	# Inspect the model in training or inference modes
	# values: 'inference' or 'training'
	# TODO: code for 'training' test mode not ready yet
	TEST_MODE = "inference"

	# def get_ax(rows=1, cols=1, size=16):
	#     """Return a Matplotlib Axes array to be used in
	#     all visualizations in the notebook. Provide a
	#     central point to control graph sizes.
	    
	#     Adjust the size attribute to control how big to render images
	#     """
	#     _, ax = plt.subplots(rows, cols, figsize=(size*cols, size*rows))
	#     return ax
	class InferenceConfig(config.__class__):
	    GPU_COUNT = 1
	    IMAGES_PER_GPU = 1

	single_image_config = InferenceConfig()

	# Create model in inference mode

	MODEL_DIR = "./RCNN/mask_rcnn_balloon_0030.h5"
	LOGS_DIR = "./RCNN/"

	with tf.device(DEVICE):
	    model = modellib.MaskRCNN(mode="inference", model_dir=LOGS_DIR,
	                              config=single_image_config)
	# Or, load the last model you trained
	weights_path = MODEL_DIR

	# Load weights
	print("Loading weights ", weights_path)
	model.load_weights(weights_path, by_name=True)
	graph = tf.get_default_graph()

	return model, graph, model_vcc, graph2, model_modelornot, graph3,model_classifymultiple, graph4
	#return loaded_model,graph