Flask
gevent
opencv-python==3.3.0.10;
gunicorn
keras
numpy
h5py
pillow
tensorflow
xlsxwriter
colormath
scikit-learn
matplotlib
scikit-image
ipython
flask-dropzone
flask_uploads
Jinja2
Werkzeug


